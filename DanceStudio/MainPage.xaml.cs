﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace DanceStudio
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /// <summary>
        /// Dance timer that gets controlled by Start / Stop dance
        /// </summary>
        private DispatcherTimer _tmDanceTimer;

        public MainPage()
        {
            this.InitializeComponent();

			//load the image avatar
			LoadDanceAvatar();

            //create and initialize the timer
            _tmDanceTimer = new DispatcherTimer();
            _tmDanceTimer.Interval = TimeSpan.FromMilliseconds(250);
            _tmDanceTimer.Tick += OnDanceShuffle;
        }


        private void OnDanceStep(object sender, RoutedEventArgs e)
        {
            const int DANCE_STEP_SIZE = 10;
            if (sender == _btnLeft)
            {
                //determine the new position of the image
                double newLeft = Canvas.GetLeft(_imgAvatar) - DANCE_STEP_SIZE;

                //move the image avatar 10 pixels to the left
                Canvas.SetLeft(_imgAvatar, newLeft);
            }
            else if (sender == _btnUp)
            {
                //determine the new position of the image
                double newTop = Canvas.GetTop(_imgAvatar) - DANCE_STEP_SIZE;

                //move the image avatar 10 pixles up
                Canvas.SetTop(_imgAvatar, newTop);

            }
            else if (sender == _btnDown)
            {
                //move the image avatar 10 pixels down
                //determine the new position of the image
                double newTop = Canvas.GetTop(_imgAvatar) + DANCE_STEP_SIZE;

                //move the image avatar 10 pixles up
                Canvas.SetTop(_imgAvatar, newTop);
            }
            else if (sender == _btnRight)
            {
                //move the image avatar 10 pixels to the right
                //determine the new position of the image
                double newLeft = Canvas.GetLeft(_imgAvatar) + DANCE_STEP_SIZE;

                //move the image avatar 10 pixels to the left
                Canvas.SetLeft(_imgAvatar, newLeft);
            }
            else if (sender == _btnRotateLeft)
            {
                //rotate the image avatar by 10 degrees left
                _imgTransform.Rotation -= DANCE_STEP_SIZE;
            }
            else if (sender == _btnRotateRight)
            {
                //rotate the image vatar by 10 degrees right
                _imgTransform.Rotation += DANCE_STEP_SIZE;
            }
            else
            {
                //Defensive programming using asserts
                Debug.Assert(false, "Unknown button for dance step. Button click will be ignored.");
            }
        }

        private void OnStartDance(object sender, RoutedEventArgs e)
        {
            if (_tmDanceTimer.IsEnabled)
            {
                //stop the timer
                _tmDanceTimer.Stop();

                //update the button caption
                _btnDance.Content = (char)Symbol.Play;
            }
            else
            {
                //start the dance timer
                _tmDanceTimer.Start();

                //update the button caption for next action
                _btnDance.Content = (char)Symbol.Pause;
            }

        }

        /// <summary>
        /// This event is triggered by the timer, every second or whatever the timer.Interval is set to
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnDanceShuffle(object sender, object e)
        {
            //change the scale factor to make the image flip horizontally
            _imgTransform.ScaleX *= -1;
        }

		private async void OnChangeAvatar(object sender, RoutedEventArgs e)
		{
			//create and configure the file open dialog
			FileOpenPicker openDlg = new FileOpenPicker();
			openDlg.FileTypeFilter.Add(".jpg");
			openDlg.FileTypeFilter.Add(".jpeg");
			openDlg.FileTypeFilter.Add(".png");
			openDlg.FileTypeFilter.Add(".bmp");
			openDlg.ViewMode = PickerViewMode.Thumbnail;
			openDlg.SuggestedStartLocation = PickerLocationId.PicturesLibrary;

			//get access to the user file
			StorageFile userAvatarFile =  await openDlg.PickSingleFileAsync();

			//check that a file was selected and copy it into Local State
			if (userAvatarFile != null)
			{
				//access the local storage
				StorageFolder localAppDataFolder = ApplicationData.Current.LocalFolder;

				//create a local state file to hold the avatar
				StorageFile  localAvatarFile = await localAppDataFolder.CreateFileAsync("DanceAvatar", 
													CreationCollisionOption.ReplaceExisting);

				//save the content of the user file into the local state file
				await userAvatarFile.CopyAndReplaceAsync(localAvatarFile);

				//load the avatar image
				LoadDanceAvatar();
			}
		}

		private async void LoadDanceAvatar()
		{
			try
			{
				//access the local state of the applicaiton
				StorageFolder localAppDataFolder = ApplicationData.Current.LocalFolder;
				StorageFile localAvatarFile = await localAppDataFolder.GetFileAsync("DanceAvatar");

				//read the file contents
				using (IRandomAccessStream fileStream = await localAvatarFile.OpenAsync(FileAccessMode.Read))
				{
					BitmapImage avatarImage = new BitmapImage();
					avatarImage.SetSourceAsync(fileStream);

					//set the image loaded as the source for the image control
					_imgAvatar.Source = avatarImage;
				}
			}
			catch (FileNotFoundException ex)
			{
				//set the avatar source to the application asset
				_imgAvatar.Source = new BitmapImage(new Uri("ms-appx:///Assets/HomerSimpson.jpg"));

				//save the application asset into local state
			}
			


		}

		private void OnImageAvatarChange(object sender, TappedRoutedEventArgs e)
		{

		}
	}
}
